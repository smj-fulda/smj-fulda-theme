# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = 'smj-fulda-theme'
  spec.version       = '0.1.0'
  spec.authors       = ['Johannes Müller']
  spec.email         = ['straightshoota@gmail.com']

  spec.summary       = %q{Theme for smj-fulda.org}
  spec.homepage      = 'http://smj-fulda.org'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(_layouts|_includes|_sass|LICENSE|README)/i}) }

  spec.add_development_dependency 'jekyll', '~> 3.8'
  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 12.3'

  spec.add_development_dependency 'i18n'
  spec.add_development_dependency 'jekyll-git_metadata'
  spec.add_development_dependency 'jekyll-paginate'
end
