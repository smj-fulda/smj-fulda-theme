---
layout: page
title: Aktionen & Projekte
permalink: /aktionen/
section: aktionen
cover_image: zeltlager.jpg
---

<figure>
    <img src="https://farm4.staticflickr.com/3875/14892120471_bd93fae304_z_d.jpg" />
    <figcaption>Mittagessen im Zeltlager</figcaption>
</figure>

Höhepunkt unserer Arbeit ist das jährliche **[Zeltlager](/zeltlager)** in den Sommerferien für Jungs von 9-13 Jahren. In diesen 12 Tagen erleben die Teilnehmer  Gemeinschaft und Abenteuer. Inhaltlich beschäftigen wir uns altersgerecht mit der Persönlichkeitsentwicklung und Übermittlung christlicher Werte. Auch die Zeit in der Natur mit einfachen Mitteln spannend zu gestalten ist für Kinder und Betreuer jedes Jahr aufs Neue eine tolle Erfahrung. Noch Jahre später werden viele Zeltlager-Geschichten erzählt.

<figure class="full-width">
    <img src="https://farm6.staticflickr.com/5559/14872129156_e192b9c98b_h_d.jpg"/>
    <figcaption>Zeltlagergemeinschaft</figcaption>
</figure>

Die **[Gemeinschaftswochenenden]({{ site.baseurl }}/gewo/)** finden mehmals im Jahr im Schönstatt-Zentrum Dietershausen statt.
Freundschaftliches Beisammensein, Spiele und andere Aktivitäten geben die Möglichkeit, das Gemeinschaftserlebnis des Zeltlagers zu erleben.

<figure>
    <img src="https://farm8.staticflickr.com/7556/15993183245_e0f15451b2_z_d.jpg" />
    <figcaption>Gottesdienst im Kapellchen an einer Kreistagung</figcaption>
</figure>
<figure>
    <img src="https://farm3.staticflickr.com/2942/15144029320_6ec99fdd4f_z_d.jpg" />
    <figcaption>Kreistagung</figcaption>
</figure>

Darauf aufbauend bieten wir für ältere Jugendliche **[Kreistagungen](aktionen/kreise)** an. Ein Kreis besteht aus einer Gruppe gleichaltriger Jugendlichen und trifft sich meist an drei Wochenenden im Jahr. Über Jahre hinweg ensteht so eine enge Gemeinschaft und Freundschaften, die über Jahrzehnte bestehen bleiben. In den Kreisen findet auch die Vorbereitung für Gruppenleiter statt und die Gemeinschaft der Kreise bildet den Grundstein der aktiven SMJ-Arbeit.


# Gemeinschaft

Abseits vom "Kerngeschäft", gibt es noch verschiedene Aktionen die wir zusammen mit Freunden und anderen Verbänden durchführen. Dazu gehören:

* Boniwallfahrt
Jedes Jahr zum Fest unseres Bistumpatrons wallen wir mit Freunden von der KJF von Johannesberg zum Grab des heiligen Bonifatius. Mit Jesus-Shirt, Gesang und guter Laune laufen wir durch die Fuldaaue und ziehen den ein oder anderen Blick auf uns. Jeder ist herzlich eingeladen mit uns zu laufen, ob mit Jesus T-Shirt oder ohne.

* Kreuzbergwanderung
Tradition ist auch die **Kreuzbergwanderung**, bei der SMJler und Freunde im Schönstattzentrum Dietershausen aufbrechen und den circa 30 km langen Pilgrweg zum Kreuzberg in der fränkischen Rhön gehen.  Lust mit zu Laufen? Meld dich bei uns!

# Projekte
<!--TODO Termine für Veranstaltungen? -->
Daneben gibt es auch zahlreiche weitere Veranstaltungen in unserem Terminkalender, die von uns oder teilweise auch in Kooperation mit der Schönstatt-Mädchenjugend und anderen Jugendgemeinschaften organisiert werden.

* Als vielleicht wichtiges Event gilt das große diözesane Jugendfest **[Fest des Glaubens](http://festdesglaubens.de/)**, dass wir seit 1996 zusammen mit der *SchönstattMJF* (Schönstattbewegung Mädchen und Junge Frauen), der *[Katholischen Jugend Fulda](http://kjf-fulda.de)* und der *[OMI-Jugend](http://omi-jugend.de)* (Jugendbüro der Oblaten der Makellosen Jungfrau Maria) jährlich organisieren.

* Gemeinsam mit der Fuldaer SchönstattMJF fahren wir jedes Jahr zur **[Nacht des Heiligtums](http://nacht-des-heiligtums.de)**, dem Jugendfestival der Schönstattjugend Deutschlands.

* In Zusammenarbei mit *[KJF](http://kjf-fulda.de)* (Katholischen Jugend Fulda), *SchönstattMJF* (Schönstattbewegung Mädchen und Junge Frauen) und *[OMI-Jugend](http://omi-jugend.de)* (Jugendbüro der Oblaten der Makellosen Jungfrau Maria) bieten wir zweimal im Jahr ein **[Glaubenskurs-Wochenende](http://glaubenskurs.bonifati.us)** für Jugendliche an. Sie bieten jungen Christen die Gelegenheit, mehr über den katholischen Glauben und seine Bedeutung für ihr Leben zu erfahren und sich darüber mit anderen Jugendlichen und jungen Erwachsenen auszutauschen. <!--TODO: Andere Einladen?, Termin? ->

<!-- TODO: Adventskalender -->
* SMS-Adventskalender

<figure class="full-width">
    <img src="https://farm6.staticflickr.com/5588/15259703615_8a08dc2c8a_o_d.jpg" />
    <figcaption>
        Fest des Glaubens
    </figcaption>
</figure>

## SMJ-Shop

Seit 2010 haben wir einen kleinen Shopo. Der Erlös aus den Verkäufen der im Shop angebotenen Produkte dient zur Unterstützung der Jugendarbeit der Schönstatt-Mannesjugend im Bistum Fulda. Die Anschaffung neuer Zelte für das SMJ-Zeltlager konnte somit unmittelbar durch die JESUS-Shirts finanziert werden. Das jährliche Zeltlager ist ein Grundpfeiler der Jugendarbeit der Schönstatt-Mannesjugend. Mit deinem Kauf unterstützt du diese Arbeit!

# Freunde
<!-- TODO
* Nacht des Heiligtums
* misiones
* Nightfever Fulda
 -->


<object data="http://static.animoto.com/swf/animotoplayer-6.31e.swf?713" height="360" id="videoPlayer" style="visibility: visible;" type="application/x-shockwave-flash" width="648"><param name="wmode" value="transparent" /><param name="allowFullScreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="allowNetworking" value="all" /><param name="flashvars" value="file=UCA1Hjtke1EInHSZKWlCvA&amp;hqfile=http%3A%2F%2Fs3-p.animoto.com%2FVideo%252FUCA1Hjtke1EInHSZKWlCvA%252F480p.mp4%3FSignature%3DrYsIl7UZ%252Fuaq7yit7IirDP6ooSc%253D%26Expires%3D1259532085%26AWSAccessKeyId%3D1MXKWT7VRC2GCM3JYP82&amp;image=auto&amp;environment=production&amp;animoto_mode=commercial&amp;autostart=false&amp;duration=215834&amp;created=1258404816&amp;calltoaction_text=&amp;calltoaction_url=&amp;id=videoPlayer" /></object>
