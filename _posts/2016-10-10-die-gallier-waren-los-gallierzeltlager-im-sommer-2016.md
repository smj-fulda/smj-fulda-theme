---
categories:
- Zeltlager
migrated:
  node_id: 1021
  migrated_at: 2017-02-21 14:43
  alias: artikel/1021-die-gallier-waren-los-gallierzeltlager-im-sommer-2016
  user_id: 1
title: Die Gallier waren los! Gallierzeltlager im Sommer 2016
created_at: 2016-10-10 16:57
excerpt: Jedes Jahr veranstaltet die Schönstatt Mannesjugend (SMJ) ein Zeltlager. Diesen Sommer stand das Lager unter dem Motto <em>Asterix und Obelix - Das verschwundene Rezept.</em> Unsere Reise in das von Römern besetzte Reich konnte nach knappen 12 Tagen erfolgreich abgeschlossen werden. Nachdem alle Gallier gemeinsam das Rezept gerettet hatten, konnte Miraculix seinen Zaubertrank brauen. Das Dorf wurde gerettet und die Römer vertrieben.
author: Jonas Wolf
image:
  cover: "!baseurl!/images/2016/2016-10-10-zeltlager-2016.jpg"
---
<p>Jedes Jahr veranstaltet die Schönstatt Mannesjugend (SMJ) ein Zeltlager. Diesen Sommer stand das Lager unter dem Motto <em>Asterix und Obelix - Das verschwundene Rezept.</em> Unsere Reise in das von Römern besetzte Reich konnte nach knappen 12 Tagen erfolgreich abgeschlossen werden. Nachdem alle Gallier gemeinsam das Rezept gerettet hatten, konnte Miraculix seinen Zaubertrank brauen. Das Dorf wurde gerettet und die Römer vertrieben.</p>
<p>Am 19. Juli begann unsere Reise in Dietershausen. Von dort aus fuhren wir in das gallische Dorf nahe Jossgrund Burgjoß, wo wir bis zum 30. Juli verweilten. Auch dieses Jahr bot der Höhepunkt unserer Jahresarbeit wieder einige Highlights. So mussten wir zum Beispiel ein Gallier-Examen bestehen, die entlegenen Orte Lettgenbrunn, Aura &amp; Fellen erkunden und das verlorene Zaubertrankrezept wiederfinden. Bei zwei Besuchen in der Sinnflut in Bad Brückenau konnten wir unsere Seetüchtigkeit unter Beweis stellen.</p>
<p>Der Abschlussabend wurde nach gallischer Sitte gebührend gefeiert, sodass am folgenden Tag doch der ein oder andere mit schwerem Herzen und einem feuchten Auge das Lager verließ.</p>
<p>Wir dürfen auf das Spektakel im nächsten Jahr gespannt sein. Dieses findet vom 4. bis 15. Juli 2017 in 37318 Thalwenden statt. Eingeladen sind alle jungen Männer zwischen 9 und 13 Jahren, die ein tolles Ferienerlebnis suchen.</p>
<p>Weitere Infos gibt&rsquo;s auf der Homepage <a href="http://smj-fulda.org">smj-fulda.org</a>. Dort wird auch die Zeltlagereinladung für nächstes Jahr veröffentlicht.</p>
