---
categories:
- SMJ-Fulda
migrated:
  node_id: 993
  migrated_at: 2017-02-20 22:28
  alias: artikel/993-noahs-tafelrunde-der-smj-fulda
  user_id: 1
title: Noahs Tafelrunde der SMJ Fulda
created_at: 2015-01-20 17:59
excerpt: Am letzten Samstag traf sich die Noahs Tafelrunde der SMJ Fulda im Schönstattzentrum in Dietershausen. Die „Noahs Tafelrunde“, so bezeichnet die SMJ Fulda ihr jährliches Planungstreffen. Das Treffen findet traditionell im Januar statt.<br> <br> In diesem Jahr kamen 11 Gruppenleiter zusammen. Die Diözesanleitung mit Steffen Büdel und Christoph Schopp leiteten das Treffen. Es wurde gemeinsam auf das Zeltlager geschaut.<br> Das Zeltlager findet in diesem Jahr vom 28. Juli bis zum 8. August in Schönstatt statt!<br> Das Thema wird „Abenteuer Leben“ sein. Es wurde ein Team gebildet, welches sich an weiteren Treffen mit der Vorbereitung des Zeltlagers beschäftigen wird.<br>
---
<p>Am letzten Samstag traf sich die Noahs Tafelrunde der SMJ Fulda im Schönstattzentrum in Dietershausen. Die &bdquo;Noahs Tafelrunde&ldquo;, so bezeichnet die SMJ Fulda ihr jährliches Planungstreffen. Das Treffen findet traditionell im Januar statt.<br />
	<br />
	In diesem Jahr kamen 11 Gruppenleiter zusammen. Die Diözesanleitung mit Steffen Büdel und Christoph Schopp leiteten das Treffen. Es wurde gemeinsam auf das Zeltlager geschaut.<br />
	Das Zeltlager findet in diesem Jahr vom 28. Juli bis zum 8. August in Schönstatt statt!<br />
	Das Thema wird &bdquo;Abenteuer Leben&ldquo; sein. Es wurde ein Team gebildet, welches sich an weiteren Treffen mit der Vorbereitung des Zeltlagers beschäftigen wird.<br />
	<br />
	Weiterhin wurde auch auf weitere Aktionen geblickt, die für die Jungen von 9 bis 13 Jahren geplant werden. <br />
	Was interessiert die Jungen? Was können wir anbieten? Tage oder Wochenenden?...<br />
	Unter anderem mit diesen Fragen gingen die Leiter daran sich Gedanken über ihre Arbeit mit den Jungs zu machen.<br />
	Das nächste Gemeinschaftswochenende findet vom 13. bis 15. Februar in Dietershausen statt.<br />
	<br />
	Außerdem schauten die Gruppenleiter auf das komplette Jahresprogramm.<br />
	Es gibt wie erwähnt Aktionen für Jungen im Alter von 9-13 Jahren. Angeboten werden aber auch Veranstaltungen für Jugendliche. Dabei ist das Fest des Glaubens, Kreistagungen, Grillfest oder &bdquo;Stammtische&hellip; zu erwähnen. <br />
	Viele Termine, die sich über ein Jahr immer wieder ansammeln. Daher ist es immer wieder schön zu sehen, wie sich junge Erwachsene in der katholischen Jugendarbeit engagieren. <br />
	Darauf kann die SMJ Fulda sehr stolz sein und sich auf das Jahr 2015 freuen!<br />
	<br />
	Zu einer Tradition gehört es auch, dass bei diesem Treffen alle Gruppenleiter ihre gesammelten Centstücke mitbringen. Diese Aktion heißt &bdquo;Pfennigfuchsaktion&ldquo;. Dabei kommt immer wieder eine große Zahl an Cents zusammen. Viele machen mit und sammeln auch gemeinsam mit Familie und Freunden. Das gesammelte Geld wird dann der SMJ gespendet!<br />
	<br />
	Bei Fragen rund um unsere Jugendarbeit stehen wir Allen gerne zur Verfügung!</p>
