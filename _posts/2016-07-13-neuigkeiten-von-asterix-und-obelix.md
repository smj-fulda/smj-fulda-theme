---
categories: []
migrated:
  node_id: 1020
  migrated_at: 2017-02-20 22:29
  alias: artikel/1020-neuigkeiten-von-asterix-und-obelix
  user_id: 1
title: Neuigkeiten von Asterix und Obelix
created_at: 2016-07-13 23:37
tagline: "<em>Es ist Sommer, ich hab das klar gemacht. Sommer ist, wenn man trotzdem lacht!</em>"
excerpt: "Es ist Sommerzeit. Für die SMJ Fulda heißt das, dass das Zeltlager vor der Tür steht. Eigentlich ein Grund zum Feiern. Doch für dieses Jahr müssen wir leider ein paar Änderungen bekannt geben. Da wir diesen Sommer leider nur 13 Anmeldungen haben (so wenige wie seit 20 Jahren nicht mehr!), müssen wir den Rahmen für unser Zeltlager ein wenig verkleinern. Wir zelten nicht wie geplant in Oberweißenbrunn, sondern in 63637 Burgjoß. Auch wenn dieses Jahr nicht so viele Jungs dabei sind, wollen wir euch aber wieder ein tolles Programm bieten und auch die geplanten 12 Tage (Dienstag bis Samstag) voll ausnutzen."
---
<div><em>Es ist Sommer, ich hab das klar gemacht. Sommer ist wenn man trotzdem lacht!</em></div>
<div>Es ist Sommerzeit. Für die SMJ Fulda heißt das, dass das Zeltlager vor der Tür steht. Eigentlich ein Grund zum Feiern. Doch für dieses Jahr müssen wir leider ein paar Änderungen bekannt geben.</div>
<div>Da wir diesen Sommer leider nur 13 Anmeldungen haben (so wenige wie seit 20 Jahren nicht mehr!), müssen wir den Rahmen für unser Zeltlager ein wenig verkleinern. Wir zelten nicht wie geplant in Oberweißenbrunn, sondern in 63637 Burgjoß.</div>
<div>Auch wenn dieses Jahr nicht so viele Jungs dabei sind, wollen wir euch aber wieder ein tolles Programm bieten und auch die geplanten 12 Tage (Dienstag bis Samstag) voll ausnutzen. Bei den Eckdaten ändert sich lediglich der Zeltplatz, an dem wir bis zum Samstag, den 30.07.2016, zelten.</div>
<div>Da wir bei so wenigen Anmeldungen unsere Dienste nur eingeschränkt wahrnehmen können, ist auch die Nachtwache für dieses Jahr eingeschränkt. Wer sich also berufen fühlt, unser Zeltlager zu überfallen, der ist für die folgenden Nächte herzlich eingeladen:</div>
<ul>
	<li>Dienstag 19.07.2016 auf Mittwoch 20.07.2016</li>
	<li>Mittwoch 20.07.2016 auf Donnerstag 21.07.2016</li>
	<li>Samstag 23.07.2016 auf Sonntag 24.07.2016</li>
	<li>Montag&nbsp; 25.07.2016 auf Dienstag 26.07.2016</li>
	<li>Mittwoch 27.07.2016 auf Donnerstag 28.07.2016</li>
	<li>Freitag 29.07.2016 auf Samstag 30.07.2016</li>
</ul>
<div>Meldet euch bitte telefonisch bei Jonas Wolf an wenn ihr überfallen wollt.</div>
