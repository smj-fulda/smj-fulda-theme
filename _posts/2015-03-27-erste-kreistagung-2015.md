---
categories:
- Kreisarbeit
migrated:
  node_id: 1002
  migrated_at: 2017-02-20 22:28
  alias: artikel/1002-erste-kreistagung-2015
  user_id: 1
tagline: Das Waldheiligtum wird ausgebaut und Kampfesspiele für Jungs zeugen von Mannsein. Drei Kreise treffen sich und erstmals wird der Senat einberufen.
image:
  cover: https://farm8.staticflickr.com/7633/16943334811_19c582b673_b.jpg
  align: top
title: Erste Kreistagung 2015
created_at: 2015-03-27 11:43
excerpt: Endlich war es wieder soweit. Das erste Kreistreffen der SMJ Fulda in Dietershausen stand an. Über 30 junge Erwachsene kamen vom 20. bis 22. März zusammen. Es trafen sich die Kreise vom Ullrich, Tobias und Steffen/Christian. Außerdem waren an diesem Wochenende weitere Männer von älteren Kreisen da um sich mal wieder zu treffen. Der Kreis vom Tobias beschäftigte sich dieses Mal mit einem aktuellen Thema was die Jungen sehr interessant fanden. Der Kreis vom Ullrich baute an diesem Wochenende ihr Kreuz auf, welches dann auch am Sonntag beim Bildstock direkt eingeweiht wurde. Das Thema vom Kreis Steffen und Christian war Mann sein. Insgesamt waren sie 14 Jungs. Am Freitagabend ging es um Vorbildsein und wer eigentlich für uns ein Vorbild sein kann.
---
<p>Endlich war es wieder soweit. Das erste Kreistreffen der SMJ Fulda in Dietershausen stand an. Über 30 junge Erwachsene kamen vom 20. bis 22. März zusammen. Es trafen sich die Kreise vom Ullrich, Tobias und Steffen/Christian. Außerdem waren an diesem Wochenende weitere Männer von älteren Kreisen da um sich mal wieder zu treffen.</p>
<p>Der Kreis vom Tobias beschäftigte sich dieses Mal mit einem aktuellen Thema was die Jungen sehr interessant fanden. Der Kreis vom Ullrich baute an diesem Wochenende ihr Kreuz auf, welches dann auch am Sonntag beim Bildstock direkt eingeweiht wurde.</p>
<p>Das Thema vom Kreis Steffen und Christian war Mann sein. Insgesamt waren sie 14 Jungs. Am Freitagabend ging es um Vorbildsein und wer eigentlich für uns ein Vorbild sein kann.</p>
<p>Nach einer &bdquo;Fackelaktion&ldquo; draußen ging es dann zu Bett. Und der Samstagmorgen folgte damit, dass sich der Kreis über die Säule Mann sein unterhalten hatte. Die Säulen sind eine wichtige Basis in der SMJ. Darunter fällt eben auch das Thema Mann sein. Wir wollen gestandene Männer werden, die für etwas Gutes einstehen.</p>
<p>Am Nachmittag wurde das Programm vom Kreis Tobias und Kreis Steffen/Christian von Rainer Gotter gestaltet. Rainer Gotter ist ein Marienbruder, der dieses Wochenende einmal bei der SMJ Fulda verbrachte.</p>
<p>Er bereitete viele Spiele für draußen vor. Bei diesen Spielen ging es um Spaß, aber auch um gruppendynamische Aktionen. Sich einander helfen, Taktiken entwickeln und gemeinsam einen Wettkampf zu haben. Während dieser Einheit tobten sich alle richtig aus und alle hatten einen riesen Spaß dabei!</p>
<p>Am Abend backte der Kreis von Steffen und Christian noch Pizza. Das war eine schöne Aktion und hat dann auch super lecker geschmeckt! Nachdem sie sich dann den Film &bdquo;Karate Kid&ldquo; angeschaut hatten, fielen alle müde ins Bett.</p>
<p>Am Sonntagmorgen war zunächst Aufräumen angesagt. Danach schauten die jungen Männer auf das Wochenende zurück. Jeder durfte sagen was ihm gefallen hat oder eben auch nicht.</p>
<p>Anschließend kam Jugendpfarrer Thomas Renze aus Fulda dazu um mit allen die Heilige Messe am Bildstock zu feiern. Nachdem Mittagessen war es schon wieder Zeit zu gehen. Die Zeit vergeht schnell an so einem Wochenende. Alle freuen sich schon wieder auf das nächste Treffen der SMJ Fulda!</p>
<figure class="flickr-photoset full-width" data-href="https://www.flickr.com/photos/45962678@N06/sets/72157651181712888">
  <a href="https://www.flickr.com/photos/45962678@N06/16944306935/in/album-72157651181712888"><img src="https://farm8.staticflickr.com/7597/16944306935_f315b5fbbd_q.jpg" alt="038--smj-kt-2015-märz" data-src-large="https://farm8.staticflickr.com/7597/16944306935_f315b5fbbd_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16944307815/in/album-72157651181712888"><img src="https://farm8.staticflickr.com/7635/16944307815_2f64cd0a26_q.jpg" alt="037--smj-kt-2015-märz" data-src-large="https://farm8.staticflickr.com/7635/16944307815_2f64cd0a26_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16943334811/in/album-72157651181712888"><img src="https://farm8.staticflickr.com/7633/16943334811_19c582b673_q.jpg" alt="036--smj-kt-2015-märz" data-src-large="https://farm8.staticflickr.com/7633/16943334811_19c582b673_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16756864380/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8707/16756864380_01489f233e_q.jpg" alt="035--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8707/16756864380_01489f233e_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16918377926/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8696/16918377926_88d1684aff_q.jpg" alt="034--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8696/16918377926_88d1684aff_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16943343711/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8718/16943343711_e311c60956_q.jpg" alt="033--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8718/16943343711_e311c60956_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16736965587/in/album-72157651181712888"><img src="https://farm8.staticflickr.com/7627/16736965587_a3841e08a7_q.jpg" alt="032--smj-kt-2015-märz" data-src-large="https://farm8.staticflickr.com/7627/16736965587_a3841e08a7_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16321918654/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8717/16321918654_8ba46c984c_q.jpg" alt="031--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8717/16321918654_8ba46c984c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16942995042/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8687/16942995042_eca1860e9c_q.jpg" alt="030--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8687/16942995042_eca1860e9c_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16918375716/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8684/16918375716_e37830af80_q.jpg" alt="029--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8684/16918375716_e37830af80_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16736963677/in/album-72157651181712888"><img src="https://farm8.staticflickr.com/7591/16736963677_cea55db8a2_q.jpg" alt="028--smj-kt-2015-märz" data-src-large="https://farm8.staticflickr.com/7591/16736963677_cea55db8a2_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16756869190/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8724/16756869190_def6ba81d6_q.jpg" alt="027--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8724/16756869190_def6ba81d6_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16324259403/in/album-72157651181712888"><img src="https://farm8.staticflickr.com/7619/16324259403_d145bf07f7_q.jpg" alt="026--smj-kt-2015-märz" data-src-large="https://farm8.staticflickr.com/7619/16324259403_d145bf07f7_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/16918373796/in/album-72157651181712888"><img src="https://farm9.staticflickr.com/8717/16918373796_d27fe92462_q.jpg" alt="025--smj-kt-2015-märz" data-src-large="https://farm9.staticflickr.com/8717/16918373796_d27fe92462_b.jpg" /></a>
  <a href="https://www.flickr.com/photos/45962678@N06/sets/72157651181712888" class="flickr-link" title="Bildergalerie Kreistagung März 2015 auf Flickr">Bildergalerie</a>
</figure>
