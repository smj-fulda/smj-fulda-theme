---
layout: page
cover_image: saeulenplatz-stencil.jpg
title: Fünf Säulen der SMJ
section: smj-fulda
---

In einem Prozess der Neugründung und Identitätsfindung
der SMJ Deutschland, wurde 1999 der Versuch unternommen, unsere
Arbeit und unsere Spiritualität zu definieren und in Worte zu
fassen. Dafür fand sich eine Gruppe von 5 jungen Männern,
die sich stellvertretend für uns und die gesamte SMJ Deutschland
dieses Projektes annahmen. Interessanterweise hatte dieses Projekt
einen Rückkopplungseffekt auf die 5 Männer, die im Rahmen
ihrer Arbeit eine so starke Gemeinschaft entwickelten. Sie
definierten sich selbst später als „Pars Motrix“
(lat. *der bewegende Teil*), weil sie sich
immer mehr der großen Bedeutung dieser Aufgabe bewusst wurden.

Das Ergebnis dieses Prozesses findet sich in den als 5 Säulen
bezeichneten Texten, die als (geistiges) Fundament unsere Arbeit
tragen und von der Jahreskonferenz der SMJ Deutschland im Dezember
1999 verabschiedet wurden.

In der Vorbereitung zum 100-jährigen Jubiläum der Schönstatt-Mannesjugend im Jahr 2012 stellten wir uns die Frage, was die SMJ nach 100 Jahren heute ausmacht. Die Antwort darauf fanden wir in den unten aufgeführten Säulentexten aus dem Jahr 1999. Zum 100-jährigen Jubiläum der SMJ wollten wir als heutige Generation ein Denkmal für die SMJ unserer Zeit errichten und planten deshalb neben dem Taborheiligtum in Vallendar einen großen Platz, an dem 5 große Steinsäulen aufgestellt wurden. Dieser Säulenplatz wurde von jungen Männern aus ganz Deutschland in Eigenarbeit gebaut und gestaltet und am 18.10.2012 im Kreise der SMJ Deutschland feierlich eingeweiht.

#TODO: BILD: SÄULEN LICHT

Als Grundpfeiler der SMJ gelten die Fünf Säulen der
Mannesjugend: *[Gemeinschaft](#gemeinschaft)*, *[Lebensschule](#lebensschule)*, *[Liebesbündnis](#liebesbündnis)*, *[Mannsein](#mannsein)* und *[Apostelsein](#apostolat)*.

## Gemeinschaft
Die SMJ ist eine von tiefen Beziehungen getragene **Gemeinschaft**.
Jeder einzelne ist ein wichtiges Mitglied und soll sich wohl fühlen
und akzeptiert werden. Die **Originalität** der
einzelnen sorgt für eine Vielfalt an Talenten, Typen und
Temperamenten und prägt den Reichtum unserer Gemeinschaft.
Jeden nehmen wir so an, wie er ist. Das Große in ihm möchten
wir fördern. In der Gemeinschaft entwickeln wir uns zu starken,
selbstständigen **Persönlichkeiten**.

Der **Glaube** an Jesus Christus trägt unser
Miteinander. Gemeinsam gelebter Glaube soll zur persönlichen
Gotteserfahrung führen. Dadurch helfen wir einander, dass jeder
seinen Zugang zu Gott findet. So entsteht junge Kirche.

> Denn wo zwei oder drei in meinem Namen versammelt sind,
da bin ich mitten unter ihnen. <small>Mt 18,20</small>

In der SMJ wird Gemeinschaft zum **Erlebnis.** Bei
unseren Veranstaltungen, Treffen und Aktionen schaffen wir die
Voraussetzungen für eine positive Gemeinschaftsatmosphäre.

Die Vision Pater Kentenichs von der ***„Neuen
Stadt“*** ist uns Auftrag und wird in unserem
Jugendzentrum in Schönstatt erlebbar, das von der SMJ
Deutschland errichtet wurde und als zentraler Sammelpunkt dient.

Das Ziel, an dem wir uns orientieren, ist die **Neue
Gemeinschaft,** *„die ein seelisches Ineinander und
Miteinander und Füreinander kennt“* (Josef
Kentenich). Jeder soll sich bei uns wohl fühlen und akzeptiert
werden

## Lebensschule
Die SMJ ist **Lebensschule** zur Entfaltung der
Person.In dieser Lebensschule geht es um jeden einzelnen. Sein Leben
steht im Mittelpunkt.

Als junge Menschen sind wir auf der Suche nach uns selbst. Die
eigenen **Stärken** und **Grenze**n
wollen wir entdecken und mit ihnen umgehen lernen. Die Entfaltung
der ganzen Person erheben wir deshalb zum Programm. Sich selbst
erziehen zu einer **festen, freien, christlichen
Persönlichkeit** ist unser Ziel.

In unserer Lebensschule lernen wir voneinander. Ältere
werden geschult, bei Jüngeren die Fragen und Entwicklungen
wahrzunehmen, die sie selbst vor wenigen Jahren durchlebt haben. Das
befähigt sie, sich für sie einzusetzen und eigene
**Erfahrungen** an sie weiterzugeben. Die Jüngeren
sollen in ihnen ein Vorbild für die eigene Lebensgestaltung
finden können. So übernimmt Jugend für Jugend
**Verantwortung.**

>Also schätzen wir von jetzt an niemand mehr nur
nach menschlichen Maßstäben ein. Wenn also jemand in
Christus ist, dann ist er eine neue Schöpfung. <small>2Kor 5,16f</small>

Die SMJ erlebt sich vor allem auf Zeltlagern, Tagungen, in den
Kreisen und Gruppen als Lebensschule. Alle Lebensbereiche der
Jugendlichen kommen hier zur Sprache.

Wir wollen Räume schaffen, in denen jeder ermutigt wird,
sich auf die Suche nach seinem **Persönlichen Ideal**
zu machen. Damit meinen wir den göttlichen Funken, der in jedem
ganz einzigartig brennt. Dieses Geschenk ist das Ziel, auf das hin
wir unser Leben ausrichten. Es ist für viele wichtig, sich in
dieser Lebensphase einen geistlichen Begleiter zu wählen, der
einen Teil des Lebens mit einem geht.

Dies ist unser Weg, Neue Menschen zu werden, denn *„der
ganze Sinn der Jugendreife ist weiter nichts als die Entwicklung des
Persönlichen Ideals“* (Josef Kentenich).


## Liebesbündnis

<figure>
    <img src="https://farm5.staticflickr.com/4001/4285143108_a589f77613_z_d.jpg" />
</figure>

Die SMJ lebt mit Maria im **Liebesbündnis**
ihre Bindung an Gott und die Menschen. Im Liebesbündnis wird
unser Glaube konkret. Dieser Glaube geht unter die Haut.

Wir wollen mit beiden Beinen auf dem Boden stehen und zugleich
mit dem Herzen in **Gott** verankert sein. Unser **Glaube**
muß alltagstauglich sein. Alles, was der einzelne erlebt, kann
für ihn ein Sprungbrett zu Gott sein. Unsere **Spiritualität**
durchdringt so all unsere Lebensbereiche. Denn Gott hat, wie es die
Bibel beschreibt, mit Mensch und Schöpfung seinen Bund
geschlossen.

Beziehungen sind der Geschmack am Leben. Je mehr ein Mensch diese
gestalten kann, umso bindungsfähiger wird er. Wir legen Wert
auf das Wachstum unserer Bindungen an Menschen, Orte und Ideale.
Dieses Netz von Bindungen erleichtert es uns, das persönliche
**Bündnis** mit Gott zu leben.

Das Liebesbündnis mit **Maria** ist unser
Schlüssel zu einem lebendigen Glauben. Wir haben sie gern und
setzen uns für sie ein. Was in der Taufe Grund gelegt ist,
vertiefen wir in unserer Beziehung zu ihr. Wir binden uns an die
Mutter und Erzieherin Jesu und wollen in ihrer Begleitung **lernen,**
als Christen zu leben. Entsprechend unserer persönlichen
Entwicklung erneuern und festigen wir unser Bündnis mit ihr.

> Als Jesus seine Mutter sah und bei ihr den Jünger,
den er liebte, sagte er zu seiner Mutter: Frau, siehe dein Sohn!
Dann sagte er zu dem Jünger: Siehe deine Mutter! Und von jener
Stunde an nahm sie der Jünger zu sich. <small>Joh 19,26-27<small>

Die Erfahrung der SMJ ist, dass unser Glaube an Orten konkret
werden will. Das **Heiligtum,** jene kleine Kapelle, in
der die ersten von uns Schönstatt gegründet haben und das
überall nachgebaut wird, ist für uns ein solcher heiliger
Raum. Hier kann man zur Ruhe kommen und fühlt sich zu Hause.
Dort wird das Liebesbündnis zur spürbaren **Energiequelle.**

In unserer Gebetskultur legen wir großen Wert auf
persönliche, frei formulierte **Gebete.** In ihnen
bringen wir unsere Lebenswelten zur Sprache und vernetzen sie mit
Gott. Mit ihm versuchen wir durch Gebete den Tag entlang in
lebendiger Verbindung zu bleiben. Diese bewusste Gestaltung des
Alltags nennen wir *Geistliche Tagesordnung.*

> Dieses Bündnis ist die Ur- und Grundform unserer
Existenz, die zündende Zielgestalt und die Grundkraft für
den ganzen Menschen in allen Situationen. <small>Josef Kentenich</small>

## Mannsein
Die SMJ schafft Raum für echtes, ganzheitliches **Mannsein.**
Ein echter Mann trainiert alle seine Seiten. Bei uns entsteht
Männerkultur, denn wir lernen miteinander und voneinander.

Die eigene Männlichkeit zu entdecken, ist für einen
Jungen ein großes **Abenteuer**. Für sein
Selbstbewusstsein ist es notwendig, dieses Profil seiner Person zu
entwickeln. Seine Kräfte, die er messen will und deren Grenzen
ihn herausfordern, gehören ebenso dazu, wie seine Fähigkeit,
etwas zu leisten und schöpferisch zu sein. Wir legen wert auf
eine unbefangene und ehrfürchtige Bejahung des eigenen Körpers.
Sie soll uns helfen, ein reifes Verhältnis zur eigenen
Sexualität zu finden.

Jungs und Mädchen brauchen für ihre Suche nach
**Identität** eigene Räume. Deshalb ist
unsere Jugendarbeit geschlechtsspezifisch. Männlichkeit kann
sich bei uns in ihrer ganzen Breite entfalten. Dazu gehören:
Sich ausprobieren, seine Gefühle zum Ausdruck bringen,
Schwächen ehrlich anerkennen. Die gemeinsamen Ziele helfen uns
Misserfolge und Grenzüberschreitungen als Erfahrungen zu
werten, die uns weiterbringen.

Die **Selbstentdeckung** als Mann hat Einfluss auf
unsere Lebensgestaltung. Wir versuchen diese an unserer persönlichen
Berufung auszurichten. Diese Berufung ist jedem einzelnen als
Lebensvision von Gott geschenkt.

> Gott schuf also den Menschen als sein Abbild; als Abbild
Gottes schuf er ihn. Als Mann und Frau schuf er sie. <small>Gen 2,27</small>

In der SMJ soll das, was in jungen Männern steckt, zum Zug
kommen. Deshalb legen wir bei unseren Treffen auch Wert auf Sport
und Spiel. Wir leiten die Teilnehmer an, kreativ und initiativ zu
werden. Darum übernehmen Jugendliche schon früh die
Verantwortung für Vorbereitung und Durchführung unserer
Veranstaltungen.

Gelungene Lebensbeispiele und Vorbilder inspirieren uns für
das eigene Leben. Ziele für die eigene Lebensgestaltung findet
der einzelne leichter, je mehr er seine Grundkräfte kennen
lernt. Das, was ihn aktuell bewegt und worin er für sich eine
Botschaft erkennt, gibt ihm für den nächsten Schritt
seiner Selbsterziehung die Richtung an. Die dabei gefassten Vorsätze
nennen wir **Partikularexamen.**

Denn der Neue Mensch ist *„vor Gott Kind, vor der Welt
aber die kraftvollste, eigenwüchsigste und eigenständigste
Mannesgestalt&rdquo;* (Josef Kentenich).

## Apostolat
Die SMJ ist **apostolisch** und baut mit an der
Zukunft von Welt und Kirche. Wir sind Apostel aus Begeisterung für
Jesus Christus. So **prägen** wir die Zukunft.

Die Zukunft liegt uns am Herzen. Das fordert uns heraus, unsere
**Gesellschaft und Kirche** mitzugestalten. Das beginnt
im Kleinen, im Alltag und heißt für uns, das Gewöhnliche
außergewöhnlich gut tun. Zukunft prägen beginnt in
unserer nächsten **Umgebung.** Wir wollen zeigen:
Leben als Christ kann gelingen.

Gemeinsam geben wir **Zeugnis,** dass christliche
Gemeinschaft begeistert. Wir bauen diese Gemeinschaft mit denen, die
dies wollen. Bei uns gibt es viele Möglichkeiten, sich dafür
einzusetzen. Was wir dabei lernen, befähigt uns für
unseren späteren Berufs- und Lebensweg. Wir werden ermutigt, in
unsere Umwelt hineinzuwirken und selbst zum Vorbild zu werden. Auf
diesem Weg tragen wir die Ideen Schönstatts in die Gesellschaft
hinein.

Wir sind überzeugt, dass Gott jeden von uns zu einer
einmaligen Aufgabe beruft. Wir sind die Jünger Christi, die er
heute als Apostel senden will. Im Liebesbündnis mit Maria
wollen wir unsere Sendung beGEISTert leben.

> Und ihr werdet meine Zeugen sein bis an die Grenzen der
Erde. <small>Apg 1,8</small>

Die SMJ ist Teil der **internationalen Schönstattbewegung.**
Das öffnet unseren Blick auch für die Jugend anderer
Länder und Kulturen. Unter den vielen Gemeinschaften unserer
Bewegung wissen wir uns mit der Schönstatt-Mädchenjugend
besonders verbunden. Das Zusammenwirken mit ihr ist uns wichtig.

In der eigenen Umgebung aktiv werden, das ist Kennzeichen eines
SMJ'lers. Wir setzen unsere Freizeit für unsere Veranstaltungen
und Projekte ein. Viele von uns engagieren sich darüber hinaus
in ihrer Kirche vor Ort, im sozialen oder politischen Bereich.
Getragen wird unser Einsatz von der Überzeugung, dass auch der
kleinste Beitrag zählt. Jeder ist wichtig und kann sich
einbringen. Er darf sich und was er kann, ganz Ernst nehmen. Denn,
tut einer etwas in Verbundenheit mit den anderen, kommt es dem
Ganzen zu Gute, selbst wenn es unsichtbar bleibt. Im Liebesbündnis
verschenken wir, als Möglichkeit fruchtbar zu werden, unser Tun
und Beten in allen Lebenssituationen. Das nennen wir Beiträge
zum **Gnadenkapital.**

Als **Neue Gemeinschaft** sind wir überzeugt,
dass *„am meisten heute diejenigen in der Welt wirken, die
das göttliche Leben, das sie in sich tragen, ausstrahlen“*
(Josef Kentenich).
