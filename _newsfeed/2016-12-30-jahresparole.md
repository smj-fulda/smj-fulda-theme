---
title: Jahresparole der SMJ-Deutschland
tags: [jako, smj, schönstatt]
author: Johannes Müller
image: "!baseurl!/images/2016/12/jahresparole-2017-crop.jpg"
---
Die Jahresparole der Schönstatt-Mannesjugend Deutschland für das Jahr 2017:


*Schönstatt wagen - Du bist gefordert*


Die Delegierten der einzelnen Diözesen haben zwischen Weihnachten und Silvester zur Jahreskonferenz im Jugendzentrum Marienberg getagt.
