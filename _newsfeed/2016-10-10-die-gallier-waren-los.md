---
author: Jonas Wolf
title: Die Gallier waren los! Gallierzeltlager im Sommer 2016
link: "!baseurl!/artikel/1021-die-gallier-waren-los-gallierzeltlager-im-sommer-2016"
---
Diesen Sommer stand das Lager unter dem Motto *Asterix und Obelix - Das verschwundene Rezept*. Unsere Reise in das von Römern besetzte Reich konnte nach knappen 12 Tagen erfolgreich abgeschlossen werden. Nachdem alle Gallier gemeinsam das Rezept gerettet hatten, konnte Miraculix seinen Zaubertrank brauen. Das Dorf wurde gerettet und die Römer vertrieben.
